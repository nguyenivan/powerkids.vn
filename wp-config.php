<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define( 'WP_MEMORY_LIMIT', '64M' );

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'vr#SBU+5DQ KZJz]dahbVBos(Lw$P*^6a0K:(dw*@NF1{<D*h~]}u}IZ-gT g85}');
define('SECURE_AUTH_KEY',  'tO.e(IwJy673]cVWSuYro8hqb(WRy/{85T|t?7G%$Z-1g3djVh91Hi(Ge{vW|DXl');
define('LOGGED_IN_KEY',    '`RGf&AOGl82.XU_OXrXh~8nvtPY:g[2[2}H=ln|UOvbAguh~n~M kiEDnBOb+1Iq');
define('NONCE_KEY',        'j+F%,{kK2x|h&Jr[+i~YNE3VA$?K&/s}x-VBdLX1)=:lTiOZE^)x|#5WyfSy`[ml');
define('AUTH_SALT',        '!~ol#XUs17=HBYUTtwZGq}&kr]TI%q|;;ifi-IBcgQ%VeZ|9`q4D3TGA)- oK,q5');
define('SECURE_AUTH_SALT', '}mo| `HcEh,%X|~pkS_*O<6%uZ&W=(&2B;cV.`g g*yqY`DWb;p/00%g8/%8sIb2');
define('LOGGED_IN_SALT',   '!}{W{QD9PEl4ne?woad7[+^W0qlfDRbV<b3Pbtm:P% D+RJIK8OJ^#[tYL0A+o!C');
define('NONCE_SALT',       'cp+Vy3jI|4ihnwe65p?&E|o1+THxZ9e#TX(h.Q<>Nxp^1lcY9pg&^+j83`=H/1cH');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'powerkids';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
